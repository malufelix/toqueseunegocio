import { HomeComponent } from "./pages/home/home.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { PostsComponent } from "./pages/posts/posts.component";

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    data: { title: 'Toque seu Negócio | Home' }
  },

  {
    path: 'posts/{id}',
    component: PostsComponent,
    data: { title: 'Toque seu Negócio'}
  },

  {
      path: '',
      component: HomeComponent,
      data: { title: 'Toque seu Negócio | Home' }
  },

  { path: '**', redirectTo: '' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
